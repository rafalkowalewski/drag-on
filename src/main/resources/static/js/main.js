$( document ).ready(function() {
    appendActiveClass()
});

function appendActiveClass() {
    $('a[href="' + location.pathname + '"]').parent().addClass('active');
}

function flipCard($this){
    $this.find(".card").toggleClass("flipped")
}

function myMap() {
    var mapElement = document.getElementById("map"),
        mapOptions = {
        center: new google.maps.LatLng(40.82238299999999,14.428905799999939),
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.TERRAIN
    }
    var map = new google.maps.Map(mapElement, mapOptions);
}