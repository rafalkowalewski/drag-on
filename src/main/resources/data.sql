INSERT INTO news (title, date, image_name, description) VALUES
('Wykluły się!', '03 Czerwiec 120r.', 'jaja.jpg', 'Są urocze, skłonne do zabawy i przyciągają uwagę odwiedzających. W ubiegły czwartek wreszcie przyszły na świat dwa potomki Złotego Smoka! Na razie są jeszcze małe i bezbronne, a już potrafią dać w kość. Trafią pod opiekę Rycerza spod herbu Trzy Kawki i jego pomocników.'),
('Poszukujemy pracownika', '29 Maj 120r.', 'pracownicy.jpg', 'W wyniku niespodziewanego pożaru pożegnaliśmy dwóch opiekunów naszych ulubieńców. Jeżeli chcesz dołączyć do naszego młodego i kreatywnego zespołu aplikuj dragon.wezuwiusz@gmail.com.'),
('Portret smoka', '28 Maj 120r.', 'wystawa.jpg', 'Serdecznie zapraszamy w imieniu Złoteg smoka na jego wystawę autoportretów, którą zrealizował na swoją cześć. Zaprezentowane zostaną jego najnowsze dzieła oraz będzie możliwość porozmawania z jego twórcą o inspiracjach i jego twórczości. Po wystawie Smoki zapowiedziały AfterParty, na które także serdecznie zapraszamy.'),
('Wyścig. Tego nie możesz przegapić!', '25 Maj 120r.', 'wyscig.jpg', 'Już 21 lipca na terenach wypożyczalni odbędzie się jedyny na świecie wyścig smoków inteligentnych o miano najszybszego z najinteligentniejszych. Złoty smok, Draco, Smok światów oraz Smaug zapowiedzieli udział w wyścigu. Podczas zawdów będzie można kupić watę cukrową i zrobić sobie pamiątkowe zdjęcie.');

INSERT INTO opinion (name, dragon_name, image_name, description) VALUES
('Dziadek z memów', 'Smaug', 'dziadek.png', 'Super smoki. Super miejsce. Obsługa przemiła. Smauga mogę polecić każdemu. Udało mi się porządnie przypiec Torina Dębową Tarczę i jego dwunastu kurdupli i tego cholernego złodzieja.'),
('Janusz', 'Złoty smok', 'janusz.png', 'Złotego smoka wypożyczyłem na miesiąc w celach czysto turystyczych. Smok jednak okazał się niesforny i spalił trzy wioski za które musiałem słono zapłacić. Nie polecam!'),
('Kratos', 'Smok światów', 'kratos.png', 'Nie rozumiałem go. Chłopcze. Idziemy...');

INSERT INTO dragon (name, dragon_class, image_name, prize, color, predaceousness, size, is_fire, age, is_speaking, movie, warden, description) VALUES
('Drogon', 'militarny', 'Drogon.jpg', 1000000, 'czarny','wysoka', 'duży', TRUE, 5, FALSE, 'Gra o tron','Daenerys Targaryen', 'Nie dajcie się zwieść pozorom! Chociaż jest jeszcze młody, spalił niejedną armię, a jego rozmiar wprawia w osłupienie wszystkich mieszkańców Westeros. Zasłynął ze zwycięztwa nad Lannisterami zeszłej zimy.' ),
('Smaug', 'militarny', 'Smaug.jpg', 1000000, 'złoty','wysoka', 'ogromny',TRUE, 2000, TRUE, 'Hobbit','brak','Najpotworniejszy i najstraszliwszy ze wszystkich smoków. Tę formułe należy mu recytować przy każdym spotkaniu, chyba że chcecie skończyć jako karkówka z grilla.' ),
('Smok światów', 'turystyczny', 'WorldSerpent.jpg', 100000000, 'błękitny','niska', 'nieskończony', FALSE, 100000, TRUE, 'God of War','brak', 'Jest podobno tak wielki, że okrąża cały świat, a jego imienia nikt z żyjących nie jest w stanie poprawnie wymówić.' ),
('Skrzydlata bestia', 'sportowy', 'Bestia.jpg', 300000, 'szary','średnia', 'mały',FALSE, 100, FALSE, 'Władca Pierścieni','Nazgul','Oślizgła i straszna bestia, która przedostała się do kultury masowej jako wierzchowiec Czarnoksiężnika z Angmaru. Na torze jest szybka jak Zygzak McQueen, a na setkę wycina Usaina Bolta.' ),
('Rogogon węgierski', 'militarny', 'Rogogon4.jpg', 750000, 'brązowy','duża', 'średni',TRUE, 15, FALSE, 'Harry Potter','Charlie Wesley','Uwielbiany na całym świecie smok z Harrego Pottera. Przeszedł depresję i załamanie nerwowe, gdy do "Insygniów Śmierci" wizęli innego smoka. Lepiej przy nim o tym nie wspominać.' ),
('Złoty smok', 'turystyczny', 'Smok.jpg', 200000, 'złoty','niska', 'duży',TRUE, 1000, TRUE, 'Wiedźmin','Trzy Kawki', '"Smoku, jesteś piękny." - Zbigniew Zamachowski' ),
('Draco', 'sportowy', 'Draco.jpg', 500000, 'brązowy','niska', 'duży', TRUE, 1000, TRUE, 'Ostatni smok','Bowen' , 'Nie wiadomo o nim zbyt wiele, bo autor tego artykułu nie oglądał filmu.');

INSERT INTO role (role) VALUES
('ADMIN'),
('USER');