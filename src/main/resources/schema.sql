DROP TABLE IF EXISTS news;
DROP TABLE IF EXISTS opinion;
DROP TABLE IF EXISTS dragons;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS user_role;

CREATE TABLE IF NOT EXISTS news (
  id          BIGINT(20)          NOT NULL        AUTO_INCREMENT,
  title       VARCHAR(255)        DEFAULT NULL,
  date        VARCHAR(255)        DEFAULT NULL,
  image_Name     VARCHAR(255)          DEFAULT NULL,
  description     VARCHAR(500)          DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS opinion (
  id          BIGINT(20) NOT NULL AUTO_INCREMENT,
  name          VARCHAR(255)        DEFAULT NULL,
  dragon_name          VARCHAR(255)        DEFAULT NULL,
  image_name          VARCHAR(255)        DEFAULT NULL,
  description          VARCHAR(255)        DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS dragon (
  id          BIGINT(20) NOT NULL AUTO_INCREMENT,
  name          VARCHAR(255)        DEFAULT NULL,
  dragon_class          VARCHAR(255)        DEFAULT NULL,
  image_name          VARCHAR(255)        DEFAULT NULL,
  prize          BIGINT   (20)     DEFAULT NULL,
  color          VARCHAR(255)        DEFAULT NULL,
  predaceousness          VARCHAR(255)        DEFAULT NULL,
  size          VARCHAR(255)        DEFAULT NULL,
  is_fire          BOOLEAN        DEFAULT NULL,
  age          BIGINT (20)        DEFAULT NULL,
  is_speaking   BOOLEAN        DEFAULT NULL,
  movie         VARCHAR(255)      DEFAULT NULL,
  warden        VARCHAR(255)      DEFAULT NULL,
  description        VARCHAR(500)      DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS user (
  user_id          BIGINT(20) NOT NULL AUTO_INCREMENT,
  first_name          VARCHAR(255) NOT NULL,
  last_name          VARCHAR(255)        NOT NULL,
  email          VARCHAR(255)        NOT NULL,
  phone_number          VARCHAR(255)        NOT NULL,
  password          VARCHAR(100)        NOT NULL,
  is_active          BOOLEAN        NOT NULL,
  PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS role (
  role_id          BIGINT(20) NOT NULL AUTO_INCREMENT,
  role          VARCHAR(255)        DEFAULT NULL,
  PRIMARY KEY (role_id)
);

CREATE TABLE IF NOT EXISTS user_role (
  user_id BIGINT(20) NOT NULL,
  role_id BIGINT(20) NOT NULL,
  PRIMARY KEY (user_id, role_id),
  FOREIGN KEY (user_id) REFERENCES user (user_id),
  FOREIGN KEY (role_id) REFERENCES role (role_id)
);