package pl.home.dragons.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.home.dragons.repository.OpinionRepository;

@Controller
public class MainController {

    @Autowired
    private OpinionRepository opinionRepository;

    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("opinions", opinionRepository.findAll());
        return "main";
    }
}
