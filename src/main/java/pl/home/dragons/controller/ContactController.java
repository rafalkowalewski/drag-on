package pl.home.dragons.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.home.dragons.model.Mail;
import pl.home.dragons.service.EmailService;

import javax.validation.Valid;

@Controller
public class ContactController {

    @Autowired
    private EmailService emailService;

    @GetMapping("/contact")
    public String contact(Model model) {
        if (!model.containsAttribute("mail")) {
            model.addAttribute("mail", new Mail());
        }
        return "contact";
    }

    @PostMapping("/contact")
    public String news(@ModelAttribute @Valid Mail mail, BindingResult bindResult, RedirectAttributes attr) {
        if (bindResult.hasErrors()) {
            attr.addFlashAttribute("org.springframework.validation.BindingResult.mail", bindResult);
            attr.addFlashAttribute("mail", mail);
            return "redirect:/contact";
        }
        emailService.sendSimpleMessage(mail);
        return "redirect:/contact";
    }
}
