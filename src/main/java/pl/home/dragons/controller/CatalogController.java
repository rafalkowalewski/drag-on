package pl.home.dragons.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.home.dragons.model.Dragon;
import pl.home.dragons.repository.DragonRepository;

import java.util.Optional;

@Controller
@RequestMapping("/catalog")
public class CatalogController {

    @Autowired
    private DragonRepository dragonRepository;

    @GetMapping
    public String catalog(Model model) {
        model.addAttribute("dragons", dragonRepository.findAll());
        return "catalog";
    }

    @GetMapping("/{id}")
    public String showProduct(@PathVariable Long id, Model model) {
        Optional<Dragon> dragonOptional = dragonRepository.findById(id);
        if (dragonOptional.isPresent()){
            model.addAttribute("dragon", dragonOptional.get());
            return "catalog-dragon";
        }
        return "redirect:/error";
    }
}
