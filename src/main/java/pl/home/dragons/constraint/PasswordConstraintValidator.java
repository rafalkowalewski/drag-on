package pl.home.dragons.constraint;

import com.google.common.collect.Lists;
import org.passay.*;
import org.passay.dictionary.WordListDictionary;
import org.passay.dictionary.WordLists;
import org.passay.dictionary.sort.ArraysSort;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.io.*;
import java.util.*;

import static java.nio.charset.StandardCharsets.UTF_8;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    private static final String INVALID_PASSWORD_LIST = "invalid-password-list.txt";
    private static final String MESSAGES = "messages.properties";

    private DictionaryRule dictionaryRule;

    @Override
    public void initialize(ValidPassword constraintAnnotation) {
        try {
            final InputStream inputStream = getInputStream(INVALID_PASSWORD_LIST);
            dictionaryRule = new DictionaryRule(
                    new WordListDictionary(
                            WordLists.createFromReader(
                                    new InputStreamReader[]{new InputStreamReader(inputStream)},
                                    true,
                                    new ArraysSort()
                            )
                    )
            );
        } catch (IOException e) {
            throw new RuntimeException("Could not load word list", e);
        }
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        final ArrayList<Rule> rules = getRules();
        final Properties props = loadProperties();

        final MessageResolver resolver = new PropertiesMessageResolver(props);
        final PasswordValidator validator = new PasswordValidator(resolver, rules);
        final RuleResult result = validator.validate(new PasswordData(password));

        if (result.isValid()) {
            return true;
        }

        final List<String> messages = validator.getMessages(result);
        final String messageTemplate = String.join(",", messages);

        context.buildConstraintViolationWithTemplate(messageTemplate)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();

        return false;
    }

    private InputStream getInputStream(String fileName) {
        return getClass().getClassLoader().getResourceAsStream(fileName);
    }

    private Properties loadProperties() {
        final Properties props = new Properties();
        try {
            props.load(new InputStreamReader(getInputStream(MESSAGES), UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }

    private ArrayList<Rule> getRules() {
        return Lists.newArrayList(
                new LengthRule(5, 100),
                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                new CharacterRule(EnglishCharacterData.LowerCase, 1),
                new CharacterRule(EnglishCharacterData.Digit, 1),
                dictionaryRule
        );
    }
}
