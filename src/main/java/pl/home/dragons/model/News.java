package pl.home.dragons.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class News {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String date;
    @Column(name = "IMAGE_NAME")
    private String imageName;
    @Column(length = 500)
    private String description;

    public News() {
    }

    public News(String title, String date, String imageName, String description) {
        this.title = title;
        this.date = date;
        this.imageName = imageName;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
