package pl.home.dragons.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Opinion {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @Column(name = "DRAGON_NAME")
    private String dragonName;
    @Column(name = "IMAGE_NAME")
    private String imageName;
    private String description;

    public Opinion() {
    }

    public Opinion(String name, String dragonName, String imageName, String description) {
        this.name = name;
        this.dragonName = dragonName;
        this.imageName = imageName;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDragonName() {
        return dragonName;
    }

    public void setDragonName(String dragonName) {
        this.dragonName = dragonName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
