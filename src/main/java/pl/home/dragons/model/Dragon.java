package pl.home.dragons.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Dragon {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @Column(name = "DRAGON_CLASS")
    private String dragonClass;
    @Column(name = "IMAGE_NAME")
    private String imageName;
    private Long prize;
    private String color;
    private String predaceousness;
    @Column(name = "IS_FIRE")
    private Boolean isFire;
    private Long age;
    @Column(name = "IS_SPEAKING")
    private Boolean isSpeaking;
    private String movie;
    private String warden;
    private String size;
    private String description;

    public Dragon() {
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDragonClass() { return dragonClass; }

    public void setDragonClass(String dragonClass) { this.dragonClass = dragonClass; }

    public String getImageName() { return imageName; }

    public void setImageName(String imageName) { this.imageName = imageName; }

    public Long getPrize() { return prize; }

    public void setPrize(Long prize) { this.prize = prize; }

    public String getColor() { return color; }

    public void setColor(String color) { this.color = color; }

    public String getPredaceousness() { return predaceousness; }

    public void setPredaceousness(String predaceousness) { this.predaceousness = predaceousness; }

    public Boolean isFire() { return isFire; }

    public void setFire(Boolean fire) { isFire = fire; }

    public Long getAge() { return age; }

    public void setAge(Long age) { this.age = age; }

    public Boolean isSpeaking() { return isSpeaking; }

    public void setSpeaking(Boolean speaking) { isSpeaking = speaking; }

    public String getMovie() { return movie; }

    public void setMovie(String movie) { this.movie = movie; }

    public String getWarden() { return warden; }

    public void setWarden(String warden) { this.warden = warden; }

    public String getSize() { return size; }

    public void setSize(String size) { this.size = size; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }
}