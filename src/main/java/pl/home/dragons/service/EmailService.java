package pl.home.dragons.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import pl.home.dragons.model.Mail;

@Service
public class EmailService {

    @Value("${spring.mail.username}")
    private String ourEmail;

    @Autowired
    private JavaMailSender emailSender;

    public void sendSimpleMessage(final Mail mail) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject(mail.getSubject());
        message.setText(mail.getContent());
        message.setFrom(mail.getEmail());
        message.setReplyTo(mail.getEmail());
        message.setTo(ourEmail);

        emailSender.send(message);
    }
}
