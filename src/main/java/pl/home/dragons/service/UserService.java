package pl.home.dragons.service;

import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.home.dragons.model.Role;
import pl.home.dragons.model.User;
import pl.home.dragons.repository.RoleRepository;
import pl.home.dragons.repository.UserRepository;

import java.util.Set;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(true);
        Role userRole = roleRepository.findByRole("ADMIN");
        user.setRoles(Sets.newHashSet(userRole));
        userRepository.save(user);
    }

    public void initAdminUser() {
        User userByEmail = findUserByEmail("test@test.pl");

        if (userByEmail == null) {
            Role admin = roleRepository.findByRole("ADMIN");
            Set<Role> adminRoleSet = Sets.newHashSet(admin);
            User user = new User(
                    "admin",
                    "admin",
                    "test@test.pl",
                    "123123123",
                    bCryptPasswordEncoder.encode("test"),
                    true,
                    adminRoleSet
            );

            userRepository.save(user);
        }
    }

}
