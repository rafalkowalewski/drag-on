package pl.home.dragons.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.home.dragons.service.UserService;

@Component
public class DatabaseLoader implements CommandLineRunner {

    @Autowired
    private UserService userService;

    @Override
    public void run(String... strings) {
        userService.initAdminUser();
    }
}