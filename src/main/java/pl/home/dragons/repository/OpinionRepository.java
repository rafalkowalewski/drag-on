package pl.home.dragons.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.home.dragons.model.Opinion;

@Repository
public interface OpinionRepository extends CrudRepository<Opinion, Long> {
}
