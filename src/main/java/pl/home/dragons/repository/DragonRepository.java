package pl.home.dragons.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.home.dragons.model.Dragon;

@Repository
public interface DragonRepository extends CrudRepository<Dragon, Long> {
}
