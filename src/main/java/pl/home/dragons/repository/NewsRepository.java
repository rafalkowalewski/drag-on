package pl.home.dragons.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.home.dragons.model.News;

@Repository
public interface NewsRepository extends CrudRepository<News, Long> {
}
